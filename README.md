<h1 align="center">MV-Pages</h1> <br>
<!--<p align="center">
    <img alt="mv-pages logo" title="mv-pages logo" src="#" width="300">
</p>-->
<div align="center">
  <strong>MV-Pages</strong>
</div>
<div align="center">
  Valais Media Library plain HTML / CSS / JS pages using [GitLab Pages](https://pages.gitlab.io).
</div>

<div align="center">
  <h3>
    <a href="https://valais-media-library.gitlab.io/mv-pages/">Access</a>
    <span> | </span>
    <a href="#documentation">Documentation</a>
    <span> | </span>
    <a href="#contributing">
      Contributing
    </a>
  </h3>
</div>

<div align="center">
  <sub>Built with ❤︎ in Valais by <a href="https://gitlab.com/valais-media-library/mv-pages/-/graphs/master">contributors</a>
</div>


## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Usage](#usage)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [History and changelog](#history-and-changelog)
- [Authors and acknowledgment](#authors-and-acknowledgment)

## Introduction

[![license](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)](https://gitlab.com/valais-media-library/mv-pages/blob/master/LICENSE)
[![pipeline status](https://gitlab.com/valais-media-library/mv-pages/badges/master/pipeline.svg)](https://gitlab.com/valais-media-library/mv-pages/commits/master)


👉 [View online](https://valais-media-library.gitlab.io/mv-pages/)

## Features

- Display static pages (HTML/CSS/JS)

## Documentation

Pages are made using pure HTML, JS and CSS. Each page is a single html file. To create a page, create a `index.html` file in a directory. Ex.: `/public/example/index.html`. You can then access it with this URL : `https://valais-media-library.gitlab.io/mv-pages/example`. If you have external JS, you can put it in the `/public/js` directory. Then add the path to the `/public/index.html` file.

### Installation

[Download the zip file](https://gitlab.com/valais-media-library/mv-pages/-/archive/master/mv-pages-master.zip) from the latest release and unzip it somewhere reachable by your webserver.

### GitLab CI

This project's static Pages are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/) every time you push or update the repository, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml). It expects to put all your HTML files in the `public/` directory.

## Contributing

We’re really happy to accept contributions from the community, that’s the main reason why we open-sourced it! There are many ways to contribute, even if you’re not a technical person. Check the [roadmap](#roadmap) or [create an issue](https://gitlab.com/valais-media-library/mv-pages/issues) if you have a question.

1. [Fork](https://help.github.com/articles/fork-a-repo/) this [project](https://gitlab.com/valais-media-library/mv-pages/)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## History and changelog

You will find changelogs and releases in the [CHANGELOG.md](https://gitlab.com/valais-media-library/mv-makerspace/blob/master/CHANGELOG) file.

## Roadmap

Nothing

## Authors and acknowledgment

* **Michael Ravedoni** - *Initial work* - [michaelravedoni](https://gitlab.com/michaelravedoni)

See also the list of [contributors](https://github.com/michaelravedoni/prathletics/contributors) who participated in this project.

## License

[MIT License](https://gitlab.com/valais-media-library/mv-pages/blob/master/LICENSE)
