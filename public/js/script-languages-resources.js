/* Set language with url parameter*/
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
if (urlParams.has("lang") && urlParams.get("lang") != "") {
  var lang = urlParams.get("lang");
} else {
  var lang = "fr";
}
//console.log(lang);

/* YAML validator: https://jsonformatter.org/yaml-formatter */
/* Template engine: https://mozilla.github.io/nunjucks/fr/templating.html*/
var yaml =
  `
qlang: ` +
  lang +
  `
i18n:
  fr:
    description: Vous trouverez ci-dessous toutes les ressources linguistiques (livres, méthodes, films, applications, etc.) proposées par la Médiathèque Valais. Ces dernières sont classées par langue d'apprentissage. Les liens renvoient au catalogue de la Médiathèque Valais ou vers la ressource elle-même.
    all: Toutes les langues
    applications: Applications mobiles
    guides: Guides
    pronunciation: Prononciation
    in_your_library: Dans votre bibliothèque en libre-accès au rayon
    online_applications: Applications en ligne
    methods: Méthodes de langue
    literature: Littérature
    written_literature: Littérature imprimée
    jung_literature: Littérature jeunesse
    comics: BD
    audio_literature: Littérature audio
    bilingual_literature: Littérature bilingue
    easy_reading: Lectures faciles
    dictionaries: Dictionnaires
    journals: Revues
    films: Films et séries
    documentary_films: Films documentaires
    fictional_films: Films de fictions
    tv_shows: Séries
    teaching_kit: Mallettes pédagogiques
  de:
    description: Nachstehend finden Sie alle sprachlichen Ressourcen (Bücher, Methoden, Filme, Anwendungen usw.), die von der Mediathek Wallis angeboten werden. Sie werden nach Lernsprache klassifiziert. Die Links verweisen auf den Katalog der Mediathek Wallis oder auf die Ressource selbst.
    all: Alle Sprachen
    applications: Mobile Anwendungen
    guides: Leitfäden
    pronunciation: Aussprache
    in_your_library: In Ihrer Bibliothek mit freiem Zugriff auf das Regal
    online_applications: Online-Anwendungen
    methods: Sprachmethoden
    literature: Literatur
    written_literature: Gedruckte Literatur
    jung_literature: Kinderliteratur
    comics: Comics
    audio_literature: Audio-Literatur
    bilingual_literature: Zweisprachige Literatur
    easy_reading: Einfaches Lesen
    dictionaries: Dictionnaires
    journals: Zeitschriften
    films: Filme und Serien
    documentary_films: Dokumentarfilme
    fictional_films: Spielfilme
    tv_shows: Serien
    teaching_kit: Bildungspaket
all:
  applications:
    - label: Duolingo
      url: https://www.duolingo.com/learn
      text: null
    - label: Memrise
      url: https://www.memrise.com
      text: null
  guides:
    - label: Forvo guides
      url: https://fr.forvo.com/guides/
      text: null
  pronunciation:
    - label: Forvo
      url: https://fr.forvo.com/languages/
      text: null
languages:
# English
 - code: en
   code_iso2: eng
   flag: 🇬🇧
   flag_code: gb
   label: English
   label_fr: Anglais
   label_de: English
   mv_call_number:
     brig: null
     sion: 1er étage → 802
     martigny: 802
     st_maurice: 802
   mv_app:
     - label: Toutapprendre.com
       url: https://biblio.toutapprendre.com/ws/authValais.aspx
       text: → Cours → Anglais
     - label: Toutapprendre.com
       url: https://biblio.toutapprendre.com/ws/authValais.aspx
       text: → Recherche → "préparation anglais"
   mv_methods:
     global: null
     fr: https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs%5B0%5D%5Bfd%5D=sub&qs%5B0%5D%5Bpr%5D=exact&qs%5B0%5D%5Bse%5D=Anglais%20%28langue%29&qs%5B0%5D%5Bop%5D=AND&lg=eng&mt=&lb=&sd=&ed=&submit=&ex=0&so=rank&fct%5Bi%5D%5Blocal17%5D%5B0%5D=Manuels%2520d%2527Enseignement
     de: https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs%5B0%5D%5Bfd%5D=sub&qs%5B0%5D%5Bpr%5D=exact&qs%5B0%5D%5Bse%5D=Anglais%20%28langue%29&qs%5B0%5D%5Bop%5D=AND&lg=eng&mt=&lb=&sd=&ed=&submit=&ex=0&so=rank&fct%5Bi%5D%5Blocal17%5D%5B0%5D=Manuels%2520pour%2520Allophones
   mv_written_literature: true
   mv_audio_literature: true
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: Lecture+facile+(anglais)
     a2: Lecture+facile+(anglais)
     b1: Lecture+facile+(anglais)
     b2: Lecture+facile+(anglais)
   mv_dictionaries: true
   mv_journals: true
   mv_films: true
   mv_teaching_kit: true 
# German
 - code: de
   code_iso2: ger
   flag: 🇩🇪
   flag_code: de
   label: Deutsch
   label_fr: Allemand
   label_de: Deutsch
   mv_call_number:
     brig: null
     sion: 1er étage → 803
     martigny: 803
     st_maurice: 803
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: true
   mv_audio_literature: true
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: Lecture facile (allemand)
     a2: Lecture facile (allemand)
     b1: Lecture facile (allemand)
     b2: Lecture facile (allemand)
   mv_dictionaries: true
   mv_journals: true
   mv_films: true
# French
 - code: fr
   code_iso2: fre
   flag: 🇫🇷
   flag_code: fr
   label: Français
   label_fr: Français
   label_de: Französisch
   mv_call_number:
     brig: null
     sion: 1er étage → 804 / 1er étage → Français facile
     martigny: 804
     st_maurice: 804
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: true
   mv_audio_literature: true
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: Lecture facile (français)
     a2: Lecture facile (français)
     b1: Lecture facile (français)
     b2: Lecture facile (français)
   mv_dictionaries: true
   mv_journals: true
   mv_films: true
# Italian
 - code: it
   code_iso2: ita
   flag: 🇮🇹
   flag_code: it
   label: Italiano
   label_fr: Italien
   label_de: Italienisch
   mv_call_number:
     brig: 805
     sion: 1er étage → 805
     martigny: 805
     st_maurice: 805
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: true
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: Lecture facile (italien)
     a2: Lecture facile (italien)
     b1: Lecture facile (italien)
     b2: Lecture facile (italien)
   mv_dictionaries: true
   mv_journals: true
   mv_films: true
# Néerlandais
 - code: nl
   code_iso2: dut
   flag: 🇳🇱
   flag_code: nl
   label: Nederlands
   label_fr: Néerlandais
   label_de: Niederländisch
   mv_call_number:
     brig: null
     sion: Ardoise → Néerlandais
     martigny: ∅
     st_maurice: ∅
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URL
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: null
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: true
   mv_journals: false
   mv_films: true
# Spanish
 - code: es
   code_iso2: es
   flag: 🇪🇸
   flag_code: es
   label: Español
   label_fr: Espagnol
   label_de: Spanisch
   mv_call_number:
     brig: null
     sion: 1er étage → 806
     martigny: 806
     st_maurice: 806
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: true
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: Lecture facile (espagnol)
     a2: Lecture facile (espagnol)
     b1: Lecture facile (espagnol)
     b2: Lecture facile (espagnol)
   mv_dictionaries: true
   mv_journals: true
   mv_films: true
# Russe
 - code: ru
   code_iso2: rus
   flag: 🇷🇺
   flag_code: ru
   label: Русский
   label_fr: Russe
   label_de: Russisch
   mv_call_number:
     brig: null
     sion: 1er étage → Ardoise → Russe/ 1er étage → 808
     martigny: 808
     st_maurice: 808
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: true
   mv_audio_literature: null
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: true
   mv_journals: false
   mv_films: true
# Arabe
 - code: ar
   code_iso2: ara
   flag: 🇦🇪
   flag_code: sa
   label: العربية
   label_fr: Arabe
   label_de: Arabisch
   mv_call_number:
     brig: null
     sion: 1er étage → Ardoise → Arabe / 1er étage → 809.2
     martigny: 809.2
     st_maurice: 809.2
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: null
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: true
   mv_journals: false
   mv_films: true
# Japonais
 - code: ja
   code_iso2: jpn
   flag: 🇯🇵
   flag_code: jp
   label: 日本語
   label_fr: Japonais
   label_de: Japanisch
   mv_call_number:
     brig: null
     sion: 1er étage → Ardoise → Japonais / 1er étage → 809.5
     martigny: ∅
     st_maurice: 809.5
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: null
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: true
   mv_journals: false
   mv_films: true
# Chinois
 - code: zh
   code_iso2: chi
   flag: 🇨🇳
   flag_code: cn
   label: 文言
   label_fr: Mandarin/Chinois
   label_de: Mandarin/Chinesisch
   mv_call_number:
     brig: null
     sion: 1er étage → Ardoise → Chinois / 1er étage → 809.5
     martigny: 809.5
     st_maurice: 809.5
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: null
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: null
   mv_journals: false
   mv_films: true
# Turc
 - code: tr
   code_iso2: tur
   flag: 🇹🇷
   flag_code: tr
   label: Türkçe
   label_fr: Turc
   label_de: Türkisch
   mv_call_number:
     brig: null
     sion: Ardoise → Turque / 809
     martigny: ∅
     st_maurice: 809
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: null
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: null
   mv_journals: false
   mv_films: true
# Portugais
 - code: pt
   code_iso2: pt
   flag: 🇵🇹
   flag_code: pt
   label: Português
   label_fr: Portugais
   label_de: Portugiesisch
   mv_call_number:
     brig: null
     sion: 1er étage → 806.90
     martigny: 806.90
     st_maurice: 806.90
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: true
   mv_audio_literature: null
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: true
   mv_journals: false
   mv_films: true
# Albanais
 - code: sq
   code_iso2: alb
   flag: 🇦🇱
   flag_code: al
   label: Shqip
   label_fr: Albanais
   label_de: Albanische
   mv_call_number:
     brig: null
     sion: ∅
     martigny: 809.1
     st_maurice: 809.1
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: true
   mv_audio_literature: null
   mv_bilingual_literature: null
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: null
   mv_journals: false
   mv_films: null
# Polonais
 - code: pl
   code_iso2: pol
   flag: 🇵🇱
   flag_code: pl
   label: Polski
   label_fr: Polonais
   label_de: Polnisch
   mv_call_number:
     brig: null
     sion: 1er étage → Ardoise → Polonais / 808
     martigny: 808
     st_maurice: 808
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: null
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: null
   mv_journals: false
   mv_films: true
# Latin
 - code: la
   code_iso2: lat
   flag: 🇻🇦
   flag_code: va
   label: Lingua Latīna
   label_fr: Latin
   label_de: Latein
   mv_call_number:
     brig: null
     sion: 1er étage → 807.1
     martigny: 807.1
     st_maurice: 807.1
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: true
   mv_journals: false
   mv_films: false
# Grec
 - code: el
   code_iso2: gre
   flag: 🇬🇷
   flag_code: gr
   label: Ελληνικά
   label_fr: Grec
   label_de: Griechisch
   mv_call_number:
     brig: null
     sion: 1er étage → 807
     martigny: 807.5
     st_maurice: 807.5
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: null
   mv_audio_literature: null
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: true
   mv_journals: false
   mv_films: false
# Hébreu
 - code: he
   code_iso2: heb
   flag: 🇮🇱
   flag_code: il
   label: עברית
   label_fr: Hébreu
   label_de: Hebräisch
   mv_call_number:
     brig: null
     sion: ∅
     martigny: 809.2
     st_maurice: 809.2
   mv_app:
     - label: null
       url: URL
       text: → Cours → XXX
     - label: null
       url: URl
       text: → Recherche → "préparation xxx"
   mv_methods:
     global: null
     fr: null
     de: null
   mv_written_literature: true
   mv_audio_literature: null
   mv_bilingual_literature: true
   mv_easy_reading:
     a1: null
     a2: null
     b1: null
     b2: null
   mv_dictionaries: null
   mv_journals: false
   mv_films: true
`;

var templateRender = `
<p class="mt-4">{{i18n[qlang].description}}</p>
<ul>
  <li><a href="#all">{{i18n[qlang].all}}</a></li>
{% for item in languages|sort(false, true, 'label_' + qlang) %}
  <li><a href="#{{ item.code }}"><span style="display: inline-block; width:30px;"><img src="https://flagcdn.com/{{ item.flag_code }}.svg" height="14" alt="flag {{ item.label }}"></span>  {{ item['label_'+qlang] }} ({{ item.label }})</a></li>
{% endfor %}
</ul>

<h2 id="all" name="all">🌍 {{i18n[qlang].all}}</h2>
<strong>📱 {{i18n[qlang].applications}}</strong>
<ul>
{% for app in all.applications %}
  <li><a href="{{ app.url }}" target="_blank">{{ app.label }}</a> {{ app.text }}</li>
{% endfor %}
</ul>
<strong>🧳 {{i18n[qlang].guides}}</strong>
<ul>
{% for item in all.guides %}
  <li><a href="{{ item.url }}" target="_blank">{{ item.label }}</a> {{ item.text }}</li>
{% endfor %}
</ul>
<strong>🎤 {{i18n[qlang].pronunciation}}</strong>
<ul>
{% for item in all.pronunciation %}
  <li><a href="{{ item.url }}" target="_blank">{{ item.label }}</a> {{ item.text }}</li>
{% endfor %}
</ul>
<div style="border-top: 3px solid black; margin:1rem 0;"></div>

{% for lang in languages %}
<h2 id="{{ lang.code }}" name="{{ lang.code }}"><img src="https://flagcdn.com/{{ lang.flag_code }}.svg" height="18" alt="flag {{ lang.label }}"> {{ lang.label }} <small>({{ lang.label_fr }} - {{ lang.label_de }})</small></h2>
<div>
<strong>{{i18n[qlang].in_your_library}}</strong> <span data-container="body" data-toggle="popover" data-placement="right" data-trigger="hover" data-html="true" data-content="DE: Regal oder Regalboden entsprechend der gewählten Sprache auf dem angegebenen Boden in der angegebenen Bibliothek.. <br>FR: Rayon ou étagère correspondant à la langue sélectionnée à l’étage indiqué dans la bibliothèque indiquée." ><i class="far fa-question-circle"></i></span> :
<ul>
{% if lang.mv_call_number.brig %}<li>Brig → {{ lang.mv_call_number.brig }}</li>{% endif %}
{% if lang.mv_call_number.sion %}<li>Sion → {{ lang.mv_call_number.sion }}</li>{% endif %}
{% if lang.mv_call_number.martigny %}<li>Martigny → {{ lang.mv_call_number.martigny }}</li>{% endif %}
{% if lang.mv_call_number.st_maurice %}<li>St-Maurice → {{ lang.mv_call_number.st_maurice }}</li>{% endif %}
</ul>
</div>
{% if lang.mv_app[0].label %}
<div>
<strong>{{i18n[qlang].online_applications}}</strong> :
<ul>
{% for app in lang.mv_app %}
{% if app.label %}<li><a href="{{ app.url }}" target="_blank">{{ app.label }}</a> {{ app.text }}</li>{% endif %}
{% endfor %}
</ul>
</div>
{% endif %}
{% if lang.mv_methods.global or lang.mv_methods.fr or lang.mv_methods.de %}
<div>
<strong>{{i18n[qlang].methods}} </strong> <span data-container="body" data-toggle="popover" data-placement="right" data-trigger="hover" data-html="true" data-content=""><i class="far fa-question-circle"></i></span> :
{% if lang.mv_methods.global %}<a href="{{ lang.mv_methods.global }}" target="_blank">&nbsp;🌍 RERO</a>{% endif %}
{% if lang.mv_methods.fr %}<a href="{{ lang.mv_methods.fr }}" target="_blank">&nbsp;🇫🇷 fr</a>{% endif %}
{% if lang.mv_methods.de %}<a href="{{ lang.mv_methods.de }}" target="_blank">&nbsp;🇩🇪 de</a>{% endif %}
</div>
{% endif %}

{% if lang.mv_written_literature or lang.mv_audio_literature or lang.mv_bilingual_literature %}
<strong>{{i18n[qlang].literature}}</strong> :
{% endif %}
{% if lang.mv_written_literature %}
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=default_scope&qs[0][fd]=lsr46&qs[0][pr]=contains&qs[0][se]=bcvsY*M*C15&qs[0][op]=OR&qs[1][fd]=lsr46&qs[1][pr]=contains&qs[1][se]=bcvsY*M*C16&qs[1][op]=OR&qs[2][fd]=lsr46&qs[2][pr]=contains&qs[2][se]=bcvsY*M*C17&qs[2][op]=AND&lg={{ lang.code_iso2 }}&mt=&lb=&sd=&ed=&submit=&ex=0&so=rank&fct[i][rtype][0]=books&so=scdate" target="_blank">📚 {{i18n[qlang].written_literature}}</a>
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=default_scope&qs[0][fd]=lsr46&qs[0][pr]=contains&qs[0][se]=bcvsY*M*C173&qs[0][op]=OR&qs[1][fd]=lsr46&qs[1][pr]=contains&qs[1][se]=bcvsY*M*C153&qs[1][op]=OR&qs[2][fd]=lsr46&qs[2][pr]=contains&qs[2][se]=bcvsY*M*C163&qs[2][op]=AND&lg={{ lang.code_iso2 }}&mt=&lb=&sd=&ed=&submit=&ex=0&so=scdate&fct[i][rtype][0]=books" target="_blank">📙 {{i18n[qlang].jung_literature}}</a>
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=default_scope&qs[0][fd]=lsr46&qs[0][pr]=contains&qs[0][se]=bcvsY*M*C18&qs[0][op]=OR&lg={{ lang.code_iso2 }}&mt=&lb=&sd=&ed=&submit=&ex=0&so=scdate" target="_blank">📔 {{i18n[qlang].comics}}</a>

{% endif %}
{% if lang.mv_audio_literature %}
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=lsr20&qs[0][pr]=exact&qs[0][se]=[Livres+audio]&qs[0][op]=AND&lg={{ lang.code_iso2 }}&mt=&lb=&sd=&ed=&submit=&ex=0&so=scdate" target="_blank" class="ml-2">🎧 {{i18n[qlang].audio_literature}}</a>
{% endif %}
{% if lang.mv_bilingual_literature %}
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=&qs[0][pr]=contains&qs[0][se]=bilingue&qs[0][op]=OR&qs[1][fd]=&qs[1][pr]=contains&qs[1][se]=zweisprachig&qs[1][op]=AND&lg={{ lang.code_iso2 }}&mt=books&lb=&sd=&ed=&submit=&ex=0&so=scdate" target="_blank" class="ml-2">📖 {{i18n[qlang].bilingual_literature}}</a>
{% endif %}
{% if lang.mv_dictionaries %}
<div>
<strong>{{i18n[qlang].dictionaries}}</strong> :
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=lsr30&qs[0][pr]=contains&qs[0][se]={{ lang.label_fr }}+(langue)&qs[0][op]=AND&qs[1][fd]=lsr20&qs[1][pr]=contains&qs[1][se]=dictionnaire&qs[1][op]=AND&lg={{ lang.code_iso2 }}&mt=books&lb=&sd=&ed=&submit=&ex=0&so=scdate" target="_blank">📘 {{i18n[qlang].dictionaries}}</a>
</div>
{% endif %}
{% if lang.mv_easy_reading.a1 or lang.mv_easy_reading.a2 or lang.mv_easy_reading.b1 or lang.mv_easy_reading.b2 %}
<div>
<strong>{{i18n[qlang].easy_reading}}</strong> :
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=&qs[0][pr]=contains&qs[0][se]=A1&qs[0][op]=AND&qs[1][fd]=&qs[1][pr]=contains&qs[1][se]={{ lang.mv_easy_reading.a1 }}&qs[1][op]=AND&lg=&mt=books&lb=&sd=&ed=&submit=&ex=0&so=rank" target="_blank">A1</a> - 
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=&qs[0][pr]=contains&qs[0][se]=A2&qs[0][op]=AND&qs[1][fd]=&qs[1][pr]=contains&qs[1][se]={{ lang.mv_easy_reading.a2 }}&qs[1][op]=AND&lg=&mt=books&lb=&sd=&ed=&submit=&ex=0&so=rank" target="_blank">A2</a> - 
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=&qs[0][pr]=contains&qs[0][se]=B1&qs[0][op]=AND&qs[1][fd]=&qs[1][pr]=contains&qs[1][se]={{ lang.mv_easy_reading.b1 }}&qs[1][op]=AND&lg=&mt=books&lb=&sd=&ed=&submit=&ex=0&so=rank" target="_blank">B1</a> - 
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=&qs[0][pr]=contains&qs[0][se]=B2&qs[0][op]=AND&qs[1][fd]=&qs[1][pr]=contains&qs[1][se]={{ lang.mv_easy_reading.b2 }}&qs[1][op]=AND&lg=&mt=books&lb=&sd=&ed=&submit=&ex=0&so=rank" target="_blank">B2</a>
</div>
{% endif %}
{% if lang.mv_journals %}
<div>
<strong>{{i18n[qlang].journals}}</strong> <span data-container="body" data-toggle="popover" data-placement="right" data-trigger="hover" data-html="true" data-content="DE: Zeitschriften, Magazine und Zeitungen. <br>FR: Magazines, revues et journaux." ><i class="far fa-question-circle"></i></span> :
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=default_scope&qs[0][fd]=sub&qs[0][pr]=contains&qs[0][se]=(langue)&qs[0][op]=AND&lg={{ lang.code_iso2 }}&mt=journals&lb=&sd=&ed=&submit=&ex=0&so=scdate" target="_blank">📖 {{i18n[qlang].journals}}</a>
</div>
{% endif %}
{% if lang.mv_films %}
<div>
<strong>{{i18n[qlang].films}}</strong> :
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=lsr20&qs[0][pr]=contains&qs[0][se]=films&qs[0][op]=AND&qs[1][fd]=lsr20&qs[1][pr]=contains&qs[1][se]=DVD&qs[1][op]=AND&lg={{ lang.code_iso2 }}&mt=dvd&lb=&sd=&ed=&submit=&ex=0&so=scdate" target="_blank">🎬 {{i18n[qlang].documentary_films}}</a> <a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=lsr20&qs[0][pr]=contains&qs[0][se]=films&qs[0][op]=AND&qs[1][fd]=lsr20&qs[1][pr]=contains&qs[1][se]=DVD&qs[1][op]=NOT&qs[2][fd]=lsr20&qs[2][pr]=contains&qs[2][se]=Films+documentaires&qs[2][op]=AND&lg={{ lang.code_iso2 }}&mt=dvd&lb=&sd=&ed=&submit=&ex=0&so=scdate" class="ml-2">🎬 {{i18n[qlang].fictional_films}}</a> <a href="https://explore.rero.ch/fr_CH/vs/result?sc=VTLS_VS_LSS&qs[0][fd]=&qs[0][pr]=contains&qs[0][se]={{ lang.label_de }}&qs[0][op]=OR&qs[4][fd]=&qs[4][pr]=contains&qs[4][se]={{ lang.label_fr }}&qs[4][op]=AND&lg=&mt=dvd&lb=&sd=&ed=&submit=&ex=0&so=scdate&fct[i][local17][0]=S%C3%A9ries%20T%C3%A9l%C3%A9vis%C3%A9es" class="ml-2">🎬 {{i18n[qlang].tv_shows}}</a>
</div>
{% endif %}
{% if lang.mv_teaching_kit %}
<div>
<strong>{{i18n[qlang].teaching_kit}}</strong> :
<a href="https://explore.rero.ch/fr_CH/vs/result?sc=default_scope&qs[0][fd]=lsr20&qs[0][pr]=exact&qs[0][se]=Mallettes+pédagogiques&qs[0][op]=AND&qs[1][fd]=sub&qs[1][pr]=exact&qs[1][se]={{ lang.label_fr }}+(langue)&qs[1][op]=AND&lg=&mt=&lb=&sd=&ed=&submit=&ex=0&so=scdate" target="_blank">🧳 {{i18n[qlang].teaching_kit}}</a>
</div>
{% endif %}
<div style="border-top: 3px solid black; margin:1rem 0;"></div>
{% endfor %}
`;

var docYaml = jsyaml.load(yaml);

var templateRender = nunjucks.renderString(templateRender, docYaml);
document.querySelector("#template").innerHTML = templateRender;

$('[data-toggle="tooltip"]').tooltip();
$('[data-toggle="popover"]').popover();

var scroll = new SmoothScroll('a[href*="#"]');
